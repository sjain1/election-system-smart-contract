pragma solidity >=0.4.25 <0.6.0;

import "./Whitelisted.sol";

contract Election is Whitelisted {
    
    struct Candidate {
        uint id;
        string name;
        uint age;
    }
    uint[] votinglist;
    uint[] public number_of_candidates;
    mapping (uint => Candidate) public candidates;
    
    uint public startCandidateRegistrationTime = 0;
    uint public endCandidateRegistrationTime = 0;
    
    uint public startVotingTime;
    uint public endVotingTime;

    uint public resultDay;
    mapping (uint => uint) votingCountOfCandidates;
    
    modifier registrationPeriod {
        require(
            startCandidateRegistrationTime <= now  && 
            now <= endCandidateRegistrationTime, 
            "Registration should be between registration period"
        );
        _;
    }

    modifier registeredCandidate(uint candidateID) {
        require(candidates[candidateID].id == candidateID, "Only registered candidate can vote");
        _;
    }
    
    modifier votingDuration {
        require(now >= startVotingTime && now <= endVotingTime, "should vote within valid voting period");
        _;
    }

    modifier resultTime {
        require(now >= resultDay, "declatre result within period");
        _;
    }
     
    function setCandidateRegistrationPeriod(
        uint startTimeStamp, 
        uint endTimeStamp
    ) 
        public 
        onlyOwner 
        returns (bool) 
    {
        require((startTimeStamp < endTimeStamp && startTimeStamp >= now), "start time should greater than current time");
        startCandidateRegistrationTime = startTimeStamp;
        endCandidateRegistrationTime = endTimeStamp;
        return true;
    } 
    
    function registerCandidate(
        string memory _name, 
        uint _age
    ) 
        public 
        registrationPeriod
        returns (bool) 
    {
        bool verify = verifyCandidate(_age);
        require(verify, "User is less then 18");

        number_of_candidates.length++;
        candidates[number_of_candidates.length - 1].id = number_of_candidates.length - 1;
        candidates[number_of_candidates.length - 1].name = _name;
        candidates[number_of_candidates.length - 1].age = _age;
        number_of_candidates[number_of_candidates.length - 1] = candidates[number_of_candidates.length - 1].id;
        
        return true; 
    }
    
    function getCandidatesCount() public view returns(uint) {
        return number_of_candidates.length;
    }

    function getCandidate(uint index) public view returns (string memory) {
        return candidates[index].name;
    }

    function setVotingPeriod(
        uint startTimeStamp, 
        uint endTimeStamp
    ) 
        public 
        onlyOwner 
        returns (bool)
    {
        require((startTimeStamp < endTimeStamp && startTimeStamp >= endCandidateRegistrationTime), "voting start time should greater than current time");
        startVotingTime = startTimeStamp;
        endVotingTime = endTimeStamp;
        return true;
    } 
    
    function vote(
        address voterId, 
        uint candidateId
    ) 
        public 
        votingDuration 
        registeredCandidate(candidateId) 
        onlyWhitelisted(voterId) returns (bool) 
    {
        votinglist.length++;
        votinglist[votinglist.length - 1] = candidateId;
        
        return true;
    }
    
    function setResultDeclationDay(uint resultDayTimeStamp) public {
        require(resultDayTimeStamp >= endVotingTime, "result day should greater than current time");
        resultDay = resultDayTimeStamp;
    }
    
    function maintainVotingCount() public resultTime onlyOwner returns (string memory) {
        require(number_of_candidates.length != 0, "No candidate existed");
        require(votinglist.length != 0, "No one gave the vote");
        uint maxCount = 0;
        uint winnerCandidateId;
        for(uint i = 0; i < votinglist.length; i++) {
            votingCountOfCandidates[votinglist[i]] += 1;
        }

        for(uint i = 0; i < number_of_candidates.length; i++) {
            if(votingCountOfCandidates[number_of_candidates[i]] >= maxCount) {
                maxCount = votingCountOfCandidates[number_of_candidates[i]];
                winnerCandidateId = number_of_candidates[i];
            }  
        }
        //return winnerCandidateId;  
        return candidates[winnerCandidateId].name;      
    }

    function verifyCandidate(uint age) internal view onlyOwner returns (bool) {
        if(age >= 18) 
            return true;
        
        return false;
    }
}
