pragma solidity >=0.4.25 <0.6.0;

import "./Ownable.sol";
/**
 * @title WhitelistedRole
 * @dev Whitelisted accounts have been approved by a WhitelistAdmin to perform certain actions (e.g. participate in a
 * crowdsale). This role is special in that the only accounts that can add it are WhitelistAdmins (who can also remove
 * it), and not Whitelisteds themselves.
 */
contract Whitelisted is Ownable {

    mapping ( address => bool ) public isWhitelisted;

    event WhitelistedAdded(address indexed account);
    event WhitelistedRemoved(address indexed account);

    modifier onlyWhitelisted(address id) {
        require(isWhitelisted[id]);
        _;
    }

    function addWhitelisted(address account) public onlyOwner {
        //require(account != address(0));
        require(!isWhitelisted[account]);
        
        isWhitelisted[account] = true;
        
        emit WhitelistedAdded(account);
    }

    function removeWhitelisted(address account) public onlyOwner onlyWhitelisted(account) {
        require(account != address(0));
        
        isWhitelisted[account] = false;
        
        emit WhitelistedRemoved(account);
    }
}
