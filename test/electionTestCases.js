var Election = artifacts.require("Election");
var Whitelisted = artifacts.require("Whitelisted");
var Ownable = artifacts.require("Ownable");
import EVMRevert from "./helpers/VMExceptionRevert";

const should = require("chai")
  .use(require("chai-as-promised"))
  .should();

contract("Election", accounts => {
  let election;
  const owner = accounts[0];
  const voter1 = accounts[1];
  const voter2 = accounts[2];
  const voter3 = accounts[3];
  //console.log(accounts[0]);
  function setEVMTime(duration) {
    return new Promise((resolve, reject) => {
      web3.currentProvider.send(
        {
          jsonrpc: "2.0",
          method: "evm_increaseTime",
          params: [duration],
          id: new Date().getSeconds()
        },
        err1 => {
          if (err1) return reject(err1);
          web3.currentProvider.send(
            {
              jsonrpc: "2.0",
              method: "evm_mine",
              params: [duration],
              id: new Date().getSeconds()
            },
            (err2, res) => {
              return err2 ? reject(err2) : resolve(res);
            }
          );
        }
      );
    });
  }

  beforeEach(async () => {
    election = await Election.new();
  });

  it("ownership", async () => {
    //await setEVMTime(Date.now());
    await election.transferOwnership(owner);
    await election
      .transferOwnership("0x0000000000000000000000000000000000000000")
      .should.be.rejectedWith(EVMRevert);
  });

  it("should decide time period for candidate registration", async () => {
    //await setEVMTime(Date.now());

    await election.registerCandidate("Div", -1, {
      from: owner,
      gasPrice: 0
    }).should.be.rejectedWith(EVMRevert);
    await election
      .setCandidateRegistrationPeriod(1548996862, 1548996862, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setCandidateRegistrationPeriod(1551267767, 1548996862, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setCandidateRegistrationPeriod(1551761662, 1552020862, {
        from: accounts[1],
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election.setCandidateRegistrationPeriod(1551761662, 1552020862, {
      from: owner,
      gasPrice: 0
    });
  });

  it("before voting candidate should do registration", async () => {
    await election.setCandidateRegistrationPeriod(1551761662, 1552020862, {
      from: owner,
      gasPrice: 0
    });

    await setEVMTime(1551790462);

    await election.registerCandidate("Sam", 26, {
      from: owner,
      gasPrice: 0
    });
    
    await election.registerCandidate("Div", 22, {
      from: owner,
      gasPrice: 0
    });

    await election.registerCandidate("John", 18, {
      from: owner,
      gasPrice: 0
    });

    await election
      .registerCandidate("Tom", 16, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await setEVMTime(Date.now());
    await election
      .registerCandidate("John", 18, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);
  });

  it("should decide time period for voting period", async () => {
    await election.setVotingPeriod(1551615369, 1551788169, {
      from: owner,
      gasPrice: 0
    });

    // await election.setVotingPeriod(1551613367, 1551347793, {
    //   from: owner,
    //   gasPrice: 0
    // }).should.be.rejectedWith(EVMRevert);

    // await election.setVotingPeriod(1548996862, 1551347793, {
    //   from: owner,
    //   gasPrice: 0
    // }).should.be.rejectedWith(EVMRevert);
  });

  it("add voters", async () => {
    await election.addWhitelisted(voter1, {
      from: owner,
      gasPrice: 0
    });

    await election.addWhitelisted(voter2, {
      from: owner,
      gasPrice: 0
    });

    await election.addWhitelisted(voter3, {
      from: owner,
      gasPrice: 0
    });

    await election
      .addWhitelisted(voter2, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);
  });

  it("remove voters", async () => {
    await election.addWhitelisted(accounts[5], {
      from: owner,
      gasPrice: 0
    });

    await election.removeWhitelisted(accounts[5], {
      from: owner,
      gasPrice: 0
    });

    const address = "0x0000000000000000000000000000000000000000";
    await election
      .removeWhitelisted(address, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .removeWhitelisted(accounts[5], {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);
  });

  it("should give the voting", async () => {
    // await election
    //   .setCandidateRegistrationPeriod(1548996862, 1548996862, {
    //     from: owner,
    //     gasPrice: 0
    //   })
    //   .should.be.rejectedWith(EVMRevert);

    // await election
    //   .setCandidateRegistrationPeriod(1551267767, 1548996862, {
    //     from: owner,
    //     gasPrice: 0
    //   })
    //   .should.be.rejectedWith(EVMRevert);

    // await election
    //   .setCandidateRegistrationPeriod(1551267767, 1548996862, {
    //     from: owner,
    //     gasPrice: 0
    //   })
    //   .should.be.rejectedWith(EVMRevert);

    await election.setCandidateRegistrationPeriod(1551761662, 1552020862, {
        from: owner,
        gasPrice: 0
      });
  
    await election
      .vote(voter1, 0, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await setEVMTime(1551790462);


    await election.registerCandidate("Sam", 26, {
      from: owner,
      gasPrice: 0
    });

    await election.registerCandidate("Div", 22, {
      from: owner,
      gasPrice: 0
    });

    await election.registerCandidate("John", 18, {
      from: owner,
      gasPrice: 0
    });
    await election
      .registerCandidate("Tom", 16, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .vote(voter1, 0, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setVotingPeriod(1551613367, 1551347793, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setVotingPeriod(1548996862, 1551347793, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election.setVotingPeriod(1552049662, 1552395262, {
      from: owner,
      gasPrice: 0
    });

    await setEVMTime(1552136062);

    await election.addWhitelisted(voter1, {
      from: owner,
      gasPrice: 0
    });

    await election.addWhitelisted(voter2, {
      from: owner,
      gasPrice: 0
    });

    await election.addWhitelisted(voter3, {
      from: owner,
      gasPrice: 0
    });

    await election.vote(voter1, 0, {
      from: owner,
      gasPrice: 0
    });

    await election.vote(voter2, 0, {
      from: owner,
      gasPrice: 0
    });

    await election.vote(voter3, 1, {
      from: owner,
      gasPrice: 0
    });

    await election
      .vote(accounts[4], 0, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);
  });

  it("should decide time period for result declaration", async () => {
    await setEVMTime(Date.now());

    await election.setResultDeclationDay(1551008567, {
      from: owner,
      gasPrice: 0
    });
  });

  it("should declare the result within the period", async () => {
    await setEVMTime(Date.now());

    await election
      .setCandidateRegistrationPeriod(1548996862, 1548996862, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setCandidateRegistrationPeriod(1551267767, 1548996862, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setCandidateRegistrationPeriod(1551267767, 1548996862, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election.setCandidateRegistrationPeriod(1551761662, 1552020862, {
        from: owner,
        gasPrice: 0
      });

    await setEVMTime(1551790462);

    await election.registerCandidate("Sam", 26, {
      from: owner,
      gasPrice: 0
    });

    await election.registerCandidate("Div", 22, {
      from: owner,
      gasPrice: 0
    });

    await election.registerCandidate("John", 18, {
      from: owner,
      gasPrice: 0
    });

    await election
      .registerCandidate("Tom", 16, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setVotingPeriod(1551613367, 1551347793, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election
      .setVotingPeriod(1548996862, 1551347793, {
        from: owner,
        gasPrice: 0
      })
      .should.be.rejectedWith(EVMRevert);

    await election.setVotingPeriod(1552049662, 1552395262, {
      from: owner,
      gasPrice: 0
    });

    await setEVMTime(1552136062);

    await election.addWhitelisted(voter1, {
      from: owner,
      gasPrice: 0
    });

    await election.addWhitelisted(voter2, {
      from: owner,
      gasPrice: 0
    });

    await election.addWhitelisted(voter3, {
      from: owner,
      gasPrice: 0
    });

    await election.vote(voter1, 0, {
      from: owner,
      gasPrice: 0
    });

    let candi = await election.getCandidate.call(1);
    console.log(candi); 

    await election.vote(voter2, 0, {
      from: owner,
      gasPrice: 0
    });
    await election.vote(voter3, 1, {
      from: owner,
      gasPrice: 0
    });

    await election.setResultDeclationDay(1552654462, {
      from: owner,
      gasPrice: 0
    });

    await setEVMTime(1552665262);

    await election.maintainVotingCount({
      from: owner,
      gasPrice: 0
    });
  });
});
